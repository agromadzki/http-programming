# HTTP Client (11/26/2019)

import sys, time, socket, os.path
from os import path

argument = sys.argv[1]

array = argument.split(':')
second_array = array[1].split('/')

host = array[0]
port = int(second_array[0])
filename = second_array[1]

separator = '.'
no_extension = filename.split(separator, 1)[0]
cached_filename = str(no_extension + "_cache.txt") # Format of cache name.

if not path.exists(cached_filename): # Check status of cache.

	# GET REQUEST: 
    
	print ('\nREQUEST:\n--------')  
    
	request = ('GET /' + filename + ' HTTP/1.1' + '\r\n' + 'Host: ' + host + ':' + str(port) + '\r\n' + '\r\n')  
	print (request)

	c_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
	c_socket.connect((host,port))  
	c_socket.send(request.encode())
	response = c_socket.recv(4096)  
	response = response.decode()   

	if 'HTTP/1.1 404' in response:
        # Not found.
		print ('RESPONSE:\n---------')   
		print(response)      
		c_socket.close()  
	else:
        # Perform initial write to cache.   
		print ('RESPONSE:\n---------')    
		print(response+'\n')    
		c_socket.close()      
		f = open(cached_filename, "w+") 
		split = response.splitlines()       
		content = str(split[6:])       
		f.write((content.strip('\'[]')) + "\n")  
		f.close()

else:    
        # Conditional GET REQUEST:  

	if "Last-Modified" in cached_filename:
        last_modified_time = cached_filename[15:]  
    
	seconds = os.path.getmtime(cached_filename)   
	t = time.gmtime(seconds) 
	last_modified_time = time.strftime("%a, %d %b %Y %H:%M:%S GMT", t)

	print ('\nCONDITIONAL REQUEST:\n--------------------')    

	request = 'GET /' + filename + ' HTTP/1.1' + '\r\n' + "Host: " + host + ':' + str(port) + '\r\n' + 'If-Modified-Since: ' + last_modified_time + '\r\n' + '\r\n'    
	print(request)   

	c_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
   	c_socket.connect((host,port))  
	c_socket.send(request.encode()) 
	response = c_socket.recv(4096)  
	response = response.decode()  
    
	if 'HTTP/1.1 304' in response: 
        # Not modified.    
    	print ('RESPONSE:\n---------')      
		print(response)      
		c_socket.close()  
	else: 
        # Write new contents to cache.    
		print ('RESPONSE:\n---------')   
		print(response+'\n')     
		c_socket.close()   
		f = open(cached_filename, "w+")   
		split = response.splitlines()      
		content = str(split[6:])      
		f.write((content.strip('\'[]')) + "\n")   
		f.close()
            
c_socket.close()
