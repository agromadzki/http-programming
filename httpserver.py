# HTTP Server (11/26/2019)

import os, sys, time, datetime, socket
address = sys.argv[1]
port = sys.argv[2]

def get_file_contents(filename):

    contents = ''    
    inputFile = open(filename, 'r', encoding='utf-8')    
    entire_file = inputFile.read()

    for line in entire_file.splitlines():        
        if '<p class="p1">' in line:            
            contents = contents + line   
            contents = contents.replace('<p class="p1">', '').replace('</p>','').replace('&lt;','<').replace('&gt;','>')    
            return contents

def change_date(filename):   

    seconds = os.path.getmtime(filename)    
    t = time.gmtime(seconds)    
    last_mod_time = time.strftime("%a, %d %b %Y %H:%M:%S GMT", t)    
    return last_mod_time
            
    # Start connection.

s_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s_socket.bind((address, int(port)))
s_socket.listen(1)
print('\nThe server is ready to receive on port: ' + str(port))

while True:    
    n_socket, address = s_socket.accept()    
    request = n_socket.recv(4096).decode()    
    t = datetime.datetime.utcnow()    
    time_format = t.strftime("%a, %d %b %Y %H:%M:%S GMT") # Current time.    
    print("\nThe request has been received.")    
    response = ""    
    split_request = request.splitlines() # Split the request into lines.    
    first_line = split_request[0].split() # Split the first line into parts.    
    slash_filename = first_line[1]    
    filename = slash_filename[1:] # Example: filename.html

    if not(os.path.isfile(filename)):        
        response += "HTTP/1.1 404 Not Found" + "\r\n" + "Date: " + time_format + "\r\n" + "\r\n"  

    elif "If-Modified-Since" in request:        
        # Conditional GET.        
        print("\n" + time_format + " <Conditional GET>")        
        time_line = split_request[2] # The line with the modified since text.        
        if_modified_since = time_line[19:] # If modified since TIME.        
        last_modified_date = change_date(filename) # Last modified date of the original file. 

        if last_modified_date > if_modified_since: # If the last modified date > the request.            
        # Standard GET.            
            content = get_file_contents(filename)            
            len_content = str(len(content))            
            response = ("HTTP/1.1 200 OK" + "" + "\r\n" + "Date: " + time_format  +"" + "\r\n" + "Last-Modified: " + last_modified_date + "" + "\r\n" +"Content-Length: " + len_content + "" + "\r\n" + "Content-Type: text/html; charset=UTF-8" + "" + "\r\n" + "" + "\r\n" + content)        
        else:            
            response = "HTTP/1.1 304 Not Modified" + "" + "\r\n" + "Date: " + last_modified_date + "" + "\r\n" + "" + "\r\n"   

    elif not "If-Modified-Since" in request:        
        print("\n" + time_format + " <Standard GET>")        
        last_modified_date = change_date(filename)        
        content = get_file_contents(filename)        
        len_content = str(len(content))        
        response = ("HTTP/1.1 200 OK" + "" + "\r\n" + "Date: " + time_format  + "" + "\r\n" + "Last-Modified: " + last_modified_date + "" + "\r\n" +"Content-Length: " + len_content + "" + "\r\n" + "Content-Type: text/html; charset=UTF-8" + "" + "\r\n" + "" + "\r\n" + content)

    n_socket.send(response.encode())    
    print("\nResponse sent to client.")    
    n_socket.close()